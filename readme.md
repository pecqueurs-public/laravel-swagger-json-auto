# Laravel Swagger JSON auto

## objective

Use Laravel capabilities to automatically create swagger doc without add phpdoc of each route. No generation needed.

## Get started

### Install

```bash
composer config repositories.gitlab.com/pecqueurs-public composer https://gitlab.com/api/v4/group/53853709/-/packages/composer/

composer require pecqueurs/laravel-swagger-json-auto
```

### Publish config

One shot :
```bash
php artisan vendor:publish --provider='PecqueurS\LaravelSwaggerJsonAuto\Providers\SwaggerProvider' --tag='config' --ansi
```

in composer.json for each update :

```json
...
"scripts": {
    ...
    "post-update-cmd": [
        ...
        "@php artisan vendor:publish --provider='PecqueurS\\LaravelSwaggerJsonAuto\\Providers\\SwaggerProvider' --tag='config' --ansi"
    ],
    ...
},
...
```

### Create swagger docker container

Add to your docker-compose file 

```yaml
    swagger:
        image: swaggerapi/swagger-ui
        ports:
        - 80:8080
        environment:
            - SWAGGER_JSON_URL=https://<project-domain>/swagger-json
            - BASE_URL=/swagger-ui
```

Change **<project-domain>** to your application domain and adapt this config to your project. If you have changed the option `base_url` on `swaggerJson.php` file config, don't forget to adapt path of `SWAGGER_JSON_URL` with your new config path modified (**swagger-json**).


### Create new route

**api.php**
```php
Route::middleware(['auth:sanctum, ability:role:admin']) // Used to know authenticated route and roles accepted
    ->get('my-route/<param>', [MyController::class, 'index']) // used to retrieve method
    ->name('my-route.index'); // used to group swagger routes and create swagger unique id 
```

First part of route **name** is used to group route on swagger. Complete name is used to create swagger route unique id.

**MyController.php**

```php
<?php

namespace App\Http\Controllers;

use PecqueurS\LaravelSwaggerJsonAuto\Attributes\Doc\Deprecated;
use PecqueurS\LaravelSwaggerJsonAuto\Attributes\Doc\Desc;
use PecqueurS\LaravelSwaggerJsonAuto\Attributes\Doc\Param;
use PecqueurS\LaravelSwaggerJsonAuto\Attributes\Doc\ResponseExample;
use PecqueurS\LaravelSwaggerJsonAuto\Attributes\Http\ErrorCode;
use App\Http\Requests\SpecificFormRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;

class MyController extends Controller
{
    #[Deprecated()] // not required
    #[Desc('Description of route')] // not required
    #[Param('string', 'specific-query-param')] // Not required
    #[ResponseExample(['response-example-format' => 'xxx'])] // not required
    #[ErrorCode(401, 'Unauthorized')] // Not required
    #[ErrorCode(422, 'Unprocessable Entity')] // Not required
    public function index(SpecificFormRequest $request, string $param) // used to retrieve path params
    {
        //
    }
}
```

**SpecificFormRequest.php**

```php
<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SpecificFormRequest extends FormRequest
{
    public function rules(): array
    {
        // Used to retrieve Query / Body params
        return [
            'param1' => 'required|string|email',
            'param2' => 'required|date'
        ];
    }
}
```

### Go further

It's possible to override or extend default swagger controller.

```php
<?php

namespace App\Http\Controllers;

use PecqueurS\LaravelSwaggerJsonAuto\Controllers\SwaggerController;

class OverriddenController extends SwaggerController
{
    // Override default
    public function swaggerJson()
    {
        $response = parent::swaggerJson();

        // Do treatment
    }

    public function swaggerJsonAnotherLanguage()
    {
        $response = parent::swaggerJson();

        // Do treatment
    }

    public function swaggerJsonAnotherVersion()
    {
        $response = parent::swaggerJson();

        // Do treatment
    }
}
```

#### Override default route

**swaggerJson.php** (config file)
```php
...
'swagger_controller_method' => [OverriddenController::class, 'swaggerJson'],
...
```

#### Add new swagger routes
**routes/api.php**
```php
Route::get(
    'swagger-another-language',
    [OverriddenController::class, 'swaggerJsonAnotherLanguage']
)->name('api.swagger-another-language');

Route::get(
    'swagger-another-version',
    [OverriddenController::class, 'swaggerJsonAnotherVersion']
)->name('api.swagger-another-version');
```

#### Change your docker config

```yaml
    swagger:
        image: swaggerapi/swagger-ui
        ports:
        - 80:8080
        environment:
            - SWAGGER_JSON_URL=https://<project-domain>/swagger-another-language
            - BASE_URL=/swagger-ui
```
