<?php

namespace PecqueurS\LaravelSwaggerJsonAuto\Providers;

use Illuminate\Support\ServiceProvider;

class SwaggerProvider extends ServiceProvider
{
    /**
     * Boot the service provider.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../Config/swaggerJson.php' => config_path('swagger_json.php'),
        ], 'config');

        $this->loadRoutesFrom(__DIR__.'/../Routes/api.php');
    }

    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../Config/swaggerJson.php', 'swagger_json'
        );
    }
}

