<?php

use Illuminate\Support\Facades\Route;



Route::middleware('api')->get(
    config('swagger_json.base_url'),
    config('swagger_json.swagger_controller_method')
)->name('api.swagger-json');
