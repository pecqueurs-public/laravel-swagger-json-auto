<?php

namespace PecqueurS\LaravelSwaggerJsonAuto\Controllers;

use PecqueurS\LaravelSwaggerJsonAuto\Attributes\Doc\Desc;
use PecqueurS\LaravelSwaggerJsonAuto\Attributes\Doc\ResponseExample;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Route as RouteFacade;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller;

use PecqueurS\LaravelSwaggerJsonAuto\Traits\LaravelRouteParser;
use PecqueurS\LaravelSwaggerJsonAuto\Traits\SwaggerRouteFormatter;

class SwaggerController extends Controller
{
    use AuthorizesRequests, ValidatesRequests;
    use LaravelRouteParser, SwaggerRouteFormatter;

    #[Desc('Get swagger route infos')]
    #[ResponseExample([
        "openapi" => "3.0.0",
        "info" => [
            "title" => "Horoscope Generator API",
            "description" => "",
            "version" => "1.0.0"
        ],
        "servers" => [
            [
            "url" => "xxx"
            ]
        ],
        "components" => [
            "securitySchemes" => [
                "bearerAuth" => [
                    "type" => "http",
                    "scheme" => "bearer",
                    "bearerFormat" => "JWT"
                ]
            ]
        ],
        "paths" => []
     ])]
    public function swaggerJson()
    {
        $result = [];
        $routes = RouteFacade::getRoutes();
        foreach ($routes as $route) {
            $item = [
                'controller' => $this->isSerializedClosure($route->getAction()) ? 'Closure' : $route->getControllerClass(),
                'keys' => '/' . $route->uri() . '.' . $this->getSwaggerMethod($route->methods() ?? []),

                'tags' => $this->getSwaggerTags($this->getVersion($route)),
                'summary' => $this->isSerializedClosure($route->getAction()) ? '' : $this->getDescription($route),
                'description' => $this->getSwaggerDescription($this->getRoleRoute($route)),
                'operationId' => $this->getVersion($route),
            ];

            if ($this->isDeprecated($route)) {
                $item['deprecated'] = true;
            }

            if ($this->isAuthRoute($route)) {
                $item['security'] = [['bearerAuth' => []]];
            }

            $pathParameters = $this->getSwaggerPathParameters($this->retrievePathParameters($route));
            if (!empty($pathParameters)) {
                $item['parameters'] = $pathParameters;
            }

            $parameters = $this->isSerializedClosure($route->getAction()) ? [] : $this->retrieveQueryOrBodyParameters($route);
            if ($this->getSwaggerMethod($route->methods() ?? []) === 'get') {
                $item['parameters'] = [...$item['parameters']??[], ...$this->getSwaggerQueryParameters($parameters)];
            } else {
                $bodyParameters = $this->getSwaggerBodyParameters($parameters);
                if (!empty($bodyParameters)) {
                    $item['requestBody'] = $bodyParameters;
                }
            }

            $responses = [];

            $responseExample = $this->isSerializedClosure($route->getAction()) ? '' : $this->getResponseExample($route);
            if (!empty($responseExample)) {
                $responses[200] = [
                    'description' => '',
                    'content' => [
                        'application/json' => [
                            'example' => json_encode(json_decode($responseExample), JSON_PRETTY_PRINT),
                        ]
                    ]
                ];
            }
            $responsesError = $this->isSerializedClosure($route->getAction()) ? [] : $this->getErrors($route);
            if (!empty($responsesError)) {
                foreach($responsesError as $code => $content) {
                    $responses[$code] = ['description' => $content];
                }
            }

            if (empty($responses)) {
                $responses[200] = [
                    'description' => '',
                    'content' => [
                        'application/json' => [
                            'example' => json_encode(json_decode('{}'), JSON_PRETTY_PRINT),
                        ]
                    ]
                ];
            }
            $item['responses'] = $responses;

            $result[] = $item;
        }

        $response = Arr::where(Arr::undot(Arr::map(Arr::pluck(
            $this->removeProtectedFields($this->filterByNamespace($result)),
            null,
            'keys'
        ), function($item) {
            unset($item['keys']);
            return $item;
        })), function ($value, $key) {
            return !empty($key);
        });

        ksort($response);

        return [
            'openapi' => "3.0.0",
            'info' => [
                'title' => config('swagger_json.title') ?? config('app.name'),
                'description' => config('swagger_json.description', ''),
                'version' => config('swagger_json.version', '1.0.0')
            ],
            'servers' => [
                [
                    'url' => config('swagger_json.url') ?? config('app.url'),
                ]
            ],
            "components" => [
                "securitySchemes" => config('swagger_json.security_schemes', [
                    "bearerAuth" => [
                        "type" => "http",
                        "scheme" => "bearer",
                        "bearerFormat" => "JWT"
                    ]
                ])
            ],
            'paths' => $response
        ];
    }


}
