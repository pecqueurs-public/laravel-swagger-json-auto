<?php

namespace PecqueurS\LaravelSwaggerJsonAuto\Traits;

use PecqueurS\LaravelSwaggerJsonAuto\Attributes\Doc\Desc;
use PecqueurS\LaravelSwaggerJsonAuto\Attributes\Doc\Param;
use PecqueurS\LaravelSwaggerJsonAuto\Attributes\Doc\ResponseExample;
use PecqueurS\LaravelSwaggerJsonAuto\Attributes\Http\ErrorCode;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Routing\Route;
use Illuminate\Support\Str;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Routing\RouteAction;
use Illuminate\Validation\Rules\Enum;
use PecqueurS\LaravelSwaggerJsonAuto\Attributes\Doc\Deprecated;

trait LaravelRouteParser {
    protected $authUserMiddleware = 'auth:sanctum';
    protected $authorizedNamespace = 'App\Http\Controllers';
    protected $fieldsToRemove = ['controller'];

    protected function retrieveQueryOrBodyParameters(Route $route): array
    {
        $parameters = [];
        foreach ($route->signatureParameters() as $param) {
            $class = (string) $param->getType();
            if (preg_match('/\\\\/', $class)) {
                if (is_a($class, FormRequest::class, true) && method_exists($class, 'rules')) {
                    $parameters = $parameters + app()->call([new $class(), 'rules']);
                } else if (is_a($class, Request::class, true)) {
                    if (method_exists($class, 'rules')) {
                        $parameters = $parameters + app()->call([request(), 'rules']);
                    }

                    if (method_exists($route->getControllerClass(), 'rules')) {
                        $parameters = $parameters + app()->call([resolve($route->getControllerClass()), 'rules']);
                    }
                }
            }
        }

        if (empty($parameters)) {
            $parameters = $this->retrieveParametersFromNativeAttributes($route);
        }

        return $this->formatRules($parameters);
    }

    protected function retrieveParametersFromNativeAttributes(Route $route): array
    {
        $attributes = $this->getAttributes($route);

        $parameters = [];
        foreach ($attributes as $attribute) {
            if (is_a($attribute->getName(), Param::class, true)) {
                $instance = $attribute->newInstance();
                $parameters[$instance->name] = [
                    $instance->optional ? 'sometimes' : 'required',
                    $instance->type,
                ];
                if ($instance->optional && !empty($instance->default)) {
                    $parameters[$instance->name][] = "default:{$instance->default}";
                }
                if (!empty($instance->description)) {
                    $parameters[$instance->name][] = "description:{$instance->description}";
                }
            }
        }

        return $parameters;
    }

    protected function formatRules(array $rules): array
    {
        return Arr::map($rules, function ($parameter) {
            if (is_string($parameter)) {
                return explode('|', $parameter);
            }

            if (is_array($parameter)) {
                return Arr::map($parameter, function ($rule) {
                    try {
                        return (string) $rule;
                    } catch (\Error $e) {
                        if ($rule instanceof Enum) {
                            $reflection = new \ReflectionClass($rule);
                            $property = $reflection->getProperty('type');
                            $property->setAccessible(true);
                            $enum = $property->getValue($rule);

                            return 'enum:' . implode(',', array_column($enum::cases(), 'value'));
                        } else if (is_object($rule)) {
                            return (new \ReflectionClass($rule))->getShortName();
                        } else {
                            return '';
                        }
                    }
                });
            }

            return $parameter;
        });
    }

    protected function retrievePathParameters(Route $route): array
    {
        $parameters = [];
        foreach ($route->signatureParameters() as $param) {
            if (!preg_match('/\\\\/', (string) $param->getType())) { // scalar attribute
                $parameters[] = [
                    'name' => $param->getName(),
                    'type' => (string) $param->getType(),
                    'optional' => $param->isOptional(),
                    'default' => $param->isDefaultValueAvailable() ? $param->getDefaultValue() : null
                ];
            } else if (is_a((string) $param->getType(), Model::class, true)) { // model attribute
                $parameters[] = [
                    'name' => $param->getName(),
                    'type' => 'model_id',
                    'optional' => $param->isOptional(),
                    'default' => $param->isDefaultValueAvailable() ? $param->getDefaultValue() : null
                ];
            }
        }
        return $parameters;
    }

    protected function isAuthRoute(Route $route): bool
    {
        return in_array(config('swagger_json.auth_middleware', $this->authUserMiddleware), $route->gatherMiddleware());
    }

    protected function getRoleRoute(Route $route): array
    {
        $findRolesByMiddlewareParsing = config('swagger_json.find_roles_by_middleware_parsing');
        if (!is_callable($findRolesByMiddlewareParsing)) {
            return [];
        }

        return $findRolesByMiddlewareParsing(collect($route->gatherMiddleware()));
    }

    protected function getVersion(Route $route)
    {
        return preg_replace('/\.(?=.*?\.)/', "::", $route->getName());
    }

    protected function getDescription(Route $route): string
    {
        $attributes = $this->getAttributes($route);
        foreach ($attributes as $attribute) {
            if (is_a($attribute->getName(), Desc::class, true)) {
                $instance = $attribute->newInstance();
                return $instance->description;
            }
        }

        return '';
    }

    protected function getErrors(Route $route): array
    {
        $attributes = $this->getAttributes($route);
        $errors = [];
        foreach ($attributes as $attribute) {
            if (is_a($attribute->getName(), ErrorCode::class, true)) {
                $instance = $attribute->newInstance();
                $errors[$instance->code] = $instance->description;
            }
        }

        return $errors;
    }

    protected function getResponseExample(Route $route): string
    {
        $attributes = $this->getAttributes($route);
        foreach ($attributes as $attribute) {
            if (is_a($attribute->getName(), ResponseExample::class, true)) {
                $instance = $attribute->newInstance();
                return $instance->json();
            }
        }

        return '';
    }

    protected function isDeprecated(Route $route): bool
    {
        $attributes = $this->getAttributes($route);
        foreach ($attributes as $attribute) {
            if (is_a($attribute->getName(), Deprecated::class, true)) {
                return true;
            }
        }

        return false;
    }

    protected function filterByNamespace(array $routes): array
    {
        return Arr::where($routes, function (array $route) {
            $authorizedNamespace = config('swagger_json.namespace', $this->authorizedNamespace);

            return (isset($route['namespace']) && Str::startsWith($route['namespace'], $authorizedNamespace))
                || (isset($route['controller']) && Str::startsWith($route['controller'], $authorizedNamespace));
        });
    }

    protected function removeProtectedFields(array $routes): array
    {
        return Arr::map($routes, function (array $route) {
            return Arr::where($route, function ($value, $key) {
                return !in_array($key, $this->fieldsToRemove);
            });
        });
    }

    /**
     * Determine if the route action is a serialized Closure.
     *
     * @return bool
     */
    protected function isSerializedClosure(array $routeAction)
    {
        return !is_string($routeAction['uses']) || RouteAction::containsSerializedClosure($routeAction);
    }

    protected function getAttributes(Route $route): array
    {
        if (class_exists($route->getControllerClass()) && method_exists($route->getControllerClass(), $route->getActionMethod())) {
            $reflectionMethod = new \ReflectionMethod($route->getControllerClass(), $route->getActionMethod());

            return $reflectionMethod->getAttributes();
        }

        return [];
    }
}
