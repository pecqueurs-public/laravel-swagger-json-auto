<?php

namespace PecqueurS\LaravelSwaggerJsonAuto\Traits;

use PecqueurS\LaravelSwaggerJsonAuto\Attributes\Doc\Desc;
use PecqueurS\LaravelSwaggerJsonAuto\Attributes\Doc\Param;
use PecqueurS\LaravelSwaggerJsonAuto\Attributes\Doc\ResponseExample;
use PecqueurS\LaravelSwaggerJsonAuto\Attributes\Http\ErrorCode;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Routing\Route;
use Illuminate\Support\Str;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Routing\RouteAction;

trait SwaggerRouteFormatter {
    protected $swaggerTypesConverter = [
        'null' => [],
        "boolean" => [
            'boolean',

        ],
        "object" => [],
        "array" => [
            'array',
            'distinct',
        ],
        "number" => [
            'decimal',
            'digits',
            'gt',
            'integer',
            'lt',
            'max_digits',
            'min_digits',
            'multiple_of',
            'numeric',
        ],
        "string" => [
            'active_url',
            'after',
            'alpha',
            'before',
            'date',
            'doesnt',
            'email',
            'ends_with',
            'ip',
            'json',
            'lowercase',
            'mac_address',
            'password',
            'starts_with',
            'string',
            'timezone',
            'uppercase',
            'url',
            'ulid',
            'uuid',

        ],
    ];


    protected function getSwaggerMethod(array $methods): string
    {
        $priority = ['GET', 'POST', 'PUT', 'DELETE'];
        foreach ($methods as $method) {
            if (in_array($method, $priority)) {
                return strtolower($method);
            }
        }

        return strtolower(reset($methods)?? '');
    }

    protected function getSwaggerTags(string $name): array
    {
        $splitted = explode('.', $name);
        return [reset($splitted) ?? ''];
    }

    protected function getSwaggerDescription(array $roles): string
    {
        $title = empty($roles) ? '' : '**Roles**: ';


        return $title . implode(', ', array_map(function($role) {
            return '_' . $role . '_';
        }, $roles));
    }

    protected function getSwaggerPathParameters(array $pathParameters): array
    {
        $result = [];
        foreach($pathParameters as $parameter) {
            $schema = [
                'type' => $this->findSwaggerType([$parameter['type']])
            ];

            if (!empty($parameter['default'])) {
                $schema['default'] = $parameter['default'];
            }

            $item = [
                'name' => Str::snake($parameter['name']),
                'in' => 'path',
                'required' => true,
                'schema' => $schema
            ];

            $result[] = $item;
        }

        return $result;
    }

    protected function getSwaggerQueryParameters(array $queryParameters): array
    {
        $result = [];
        foreach($queryParameters as $name => $rules) {
            $schema = [
                'type' => $this->findSwaggerType($rules)
            ];

            $rulesToAdd = array_filter($rules, function($rule) {
                return !in_array($rule, [...array_keys($this->swaggerTypesConverter), ...['required']]);
            });
            $rulesTitle = empty($rulesToAdd) ? '' : "**Rules**: ";

            $item = [
                'name' => $name,
                'in' => 'query',
                'required' => in_array('required', $rules),
                'schema' => $schema,
                'description' => $rulesTitle . implode('|', array_map(function($rule) {
                    return '_' . $rule . '_';
                }, $rulesToAdd))
            ];

            $result[] = $item;
        }

        return $result;
    }

    protected function getSwaggerBodyParameters(array $bodyParameters): array
    {
        $required = [];
        $properties = [];
        foreach($bodyParameters as $name => $rules) {
            if (in_array('required', $rules)) {
                $required[] = $name;
            }
            $type = $this->findSwaggerType($rules);

            $rulesToAdd = array_filter($rules, function($rule) {
                return !in_array($rule, [...array_keys($this->swaggerTypesConverter), ...['required']]);
            });
            $rulesTitle = empty($rulesToAdd) ? '' : "**Rules**: ";

            $properties[$name] = [
                'type' => $type,
                'description' => $rulesTitle . implode('|', array_map(function($rule) {
                    return '_' . $rule . '_';
                }, $rulesToAdd))
            ];
        }

        $result = [];
        if (!empty($bodyParameters)) {
            $result = [
                'content' => [
                    'application/json' => [
                        'schema' => [
                            'type' => 'object',
                            'required' => $required,
                            'properties' => $properties,
                        ]
                    ]
                ]
            ];
        }

        return $result;
    }

    protected function findSwaggerType(array $rules, string $default = 'string'): string
    {
        foreach($rules as $rule) {
            foreach($this->swaggerTypesConverter as $swaggerType => $laravelTypes) {
                if (in_array($rule, $laravelTypes)) {
                    return $swaggerType;
                }
            }
        }

        return $default;
    }
}
