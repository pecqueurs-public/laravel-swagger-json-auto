<?php

use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use PecqueurS\LaravelSwaggerJsonAuto\Controllers\SwaggerController;

return [

    /*
    |--------------------------------------------------------------------------
    | Swagger controller
    |--------------------------------------------------------------------------
    |
    | Controller to use for route. Default can be overridden for specific usage
    |
    */

    'swagger_controller_method' => [SwaggerController::class, 'swaggerJson'],


    /*
    |--------------------------------------------------------------------------
    | Url
    |--------------------------------------------------------------------------
    |
    | This variable override default value set by config('app.url')
    |
    */

    'url' => null,


    /*
    |--------------------------------------------------------------------------
    | Base url
    |--------------------------------------------------------------------------
    |
    | Access point to swagger json route
    |
    */

    'base_url' => 'swagger-json',


    /*
    |--------------------------------------------------------------------------
    | Namespace
    |--------------------------------------------------------------------------
    |
    | Namespace controllers visible in swagger
    |
    */

    'namespace' => 'App\Http\Controllers',


    /*
    |--------------------------------------------------------------------------
    | Auth middleware
    |--------------------------------------------------------------------------
    |
    | Auth middleware used to retrieve authenticated routes
    |
    */

    'auth_middleware' => 'auth:sanctum',


    /*
    |--------------------------------------------------------------------------
    | Title
    |--------------------------------------------------------------------------
    |
    | This variable override default value set by config('app.name')
    |
    */

    'title' => null,


    /*
    |--------------------------------------------------------------------------
    | Description
    |--------------------------------------------------------------------------
    |
    | swagger description
    |
    */

    'description' => '',


    /*
    |--------------------------------------------------------------------------
    | Version
    |--------------------------------------------------------------------------
    |
    | swagger version
    |
    */

    'version' => '1.0.0',


    /*
    |--------------------------------------------------------------------------
    | securitySchemes
    |--------------------------------------------------------------------------
    |
    | swagger version
    |
    */

    'security_schemes' => [
        "bearerAuth" => [
            "type" => "http",
            "scheme" => "bearer",
            "bearerFormat" => "JWT"
        ]
    ],


    /*
    |--------------------------------------------------------------------------
    | Find Roles By Middleware Parsing
    |--------------------------------------------------------------------------
    |
    | Function return array list of roles used for specific route. Depends of middleware used.
    | By default auth system used is sanctum, roles can be setted for each route with ability/abilities midlewares
    | see https://laravel.com/docs/10.x/sanctum#token-ability-middleware for more informations
    |
    */

    'find_roles_by_middleware_parsing' => function(Collection $middlewareList): array {
        return $middlewareList->filter(function ($item) {
            return Str::startsWith($item, 'ability:') || Str::startsWith($item, 'abilities:');
        })->map(function ($item) {
            if (Str::startsWith($item, 'ability:')) {
                return Str::of($item)->replace('ability:', '')->explode(',')->toArray();
            } else {
                return Str::of($item)->replace('abilities:', '')->explode(',')->toArray();
            }
        })->flatten()->map(function ($item) {
            return Str::of($item)->replace('role:', '')->toString();
        })->toArray();
    }
];
