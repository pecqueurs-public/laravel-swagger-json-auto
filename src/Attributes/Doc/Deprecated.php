<?php

namespace PecqueurS\LaravelSwaggerJsonAuto\Attributes\Doc;

use Attribute;

#[Attribute(Attribute::IS_REPEATABLE | Attribute::TARGET_METHOD)]
class Deprecated
{
    public function __construct(){}
}
