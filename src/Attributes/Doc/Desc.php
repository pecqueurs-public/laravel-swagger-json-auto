<?php

namespace PecqueurS\LaravelSwaggerJsonAuto\Attributes\Doc;

use Attribute;

#[Attribute(Attribute::IS_REPEATABLE | Attribute::TARGET_METHOD)]
class Desc
{
    public function __construct(
        public string $description = ''
    ){}
}
