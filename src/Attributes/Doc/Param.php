<?php

namespace PecqueurS\LaravelSwaggerJsonAuto\Attributes\Doc;

use Attribute;

#[Attribute(Attribute::IS_REPEATABLE | Attribute::TARGET_METHOD)]
class Param
{
    public function __construct(
        public string $type,
        public string $name,
        public bool $optional = false,
        public mixed $default = null,
        public string $description = '',
        public array $rules = [],
    ){}
}
