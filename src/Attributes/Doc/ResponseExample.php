<?php

namespace PecqueurS\LaravelSwaggerJsonAuto\Attributes\Doc;

use Attribute;

#[Attribute(Attribute::IS_REPEATABLE | Attribute::TARGET_METHOD)]
class ResponseExample
{
    public function __construct(
        public mixed $example = []
    ){}

    public function json()
    {
        if (is_string($this->example)) {
            return $this->example;
        }

        return json_encode($this->example, JSON_UNESCAPED_UNICODE);
    }
}
