<?php

namespace PecqueurS\LaravelSwaggerJsonAuto\Attributes\Http;

use Attribute;

#[Attribute(Attribute::IS_REPEATABLE | Attribute::TARGET_METHOD)]
class ErrorCode
{
    public function __construct(
        public int $code,
        public string $description = ''
    ){}
}
